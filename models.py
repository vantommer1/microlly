from peewee import *
import datetime
import click
from flask_login import UserMixin

database = SqliteDatabase("data.sqlite3")

class BaseModel(Model):

    class Meta:
        database = database

class User(UserMixin, BaseModel):

    username = CharField()
    first_name = CharField()
    last_name = CharField()
    email = CharField()
    password = CharField()
    created_at = DateTimeField(default=datetime.datetime.now)

class Publication(BaseModel):

    title = CharField()
    body = CharField()
    created_at = DateTimeField(default=datetime.datetime.now)
    updated_at = DateTimeField(default=datetime.datetime.now)
    user = ForeignKeyField(User, backref="publications")


def create_tables():
    with database:
        database.create_tables([User, Publication])


def drop_tables():
    with database:
        database.drop_tables([User, Publication])
