from flask import Flask, url_for, render_template, request, flash, redirect
from models import *
from forms import *
from flask_login import LoginManager, login_user, login_required, logout_user, current_user
app = Flask(__name__)
app.secret_key = 'Blah'
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

@login_manager.user_loader
def user_load(user_id):
    return User.get(user_id)

@app.route('/', methods=['GET'])
def index():
    post = Publication.select()
    return render_template('index.html', post=post)

@app.route('/register', methods=['GET', 'POST', ])
def register():
    user = User()
    form = RegisterUserForm()
    if request.method == 'POST':
        form = RegisterUserForm(request.form)
        if form.validate():
            form.populate_obj(user)
            user.save()
            flash('Utilisateur crée !')
    return render_template('register.html', form=form)

@app.route('/login', methods=['GET', 'POST', ])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.get_or_none(
            User.username == form.username.data, User.password == form.password.data)
        if user:
            login_user(user)
            flash('Bienvenue parmis nous ' + current_user.username + '!')
            return redirect(url_for('connecte'))
        else:
            flash('Invalid credential please try again')
    return render_template('login.html', form=form)

@app.route('/connecte', methods=['GET'])
@login_required
def connecte():
    post = Publication.select().where(Publication.user == current_user.id)
    return render_template('connecte.html', post=post)

@app.route('/deconnecte', methods=['GET'])
@login_required
def deconnecte():
    logout_user()
    flash('Deconnexion')
    return redirect(url_for('index'))


@app.route('/New', methods=['GET', 'POST', ])
@login_required
def New():
    post = Publication()
    form = PublicationForm()
    if request.method == 'POST':
        form = PublicationForm(request.form)
        if form.validate():
            form.populate_obj(post)
            post.user = current_user.id
            post.save()
            flash('Posté !')
    return render_template('New.html', form=form)


@app.cli.command()
def initdb():
    """Create database"""
    create_tables()
    click.echo('Initialized the database')


@app.cli.command()
def dropdb():
    """Drop database tables"""
    drop_tables()
    click.echo('Dropped tables from database')
